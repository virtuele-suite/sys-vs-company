INSERT INTO vs_company.company (created_by,created_date,last_modified_date,version,company_description,company_name,company_contact_id) VALUES 
(1,'2021-04-13 08:47:14.0','2021-04-13 08:47:14.0',0,'Detailing and Modelling Services','The Acme Engineering Services',1)
,(1,'2021-04-13 08:48:05.0','2021-04-13 08:48:05.0',0,'Enginering Consulting, Detailing and Modelling Services','Star Seven Engineering Consulting',2)
,(2,'2021-04-13 08:49:40.0','2021-04-13 08:49:40.0',0,'Enginering Consulting, Detailing and Modelling Services','SANRA Engineering Consulting',3)
;
INSERT INTO vs_company.company_contact (created_by,created_date,last_modified_date,version,address_line1,address_line2,address_line3,city,country,primary_contact_number,primary_email,secondary_contact_number,secondary_email,state,zip_code) VALUES 
(1,'2021-04-13 08:47:14.0','2021-04-13 08:47:14.0',0,'Parke Avenue',"#36",'Baker Street','London','UK','9887477373','goran.evan@gmail.com','8447399393','linda.goodman@gmail.com',7,'84883')
,(1,'2021-04-13 08:48:05.0','2021-04-13 08:48:05.0',0,'Parke Avenue',"#36",'Baker Street','London','UK','9887477373','goran.evan@gmail.com','8447399393','linda.goodman@gmail.com',7,'84883')
,(2,'2021-04-13 08:49:40.0','2021-04-13 08:49:40.0',0,'Hebbal Industrial Area',"#168",'Opposite to Infosis','Mysore','IN','8447464789','mahesh.n@gmail.com','9878457755','jsp.t@gmail.com',7,'84883')
;