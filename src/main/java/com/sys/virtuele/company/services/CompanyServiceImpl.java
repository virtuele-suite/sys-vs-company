package com.sys.virtuele.company.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sys.virtuele.company.domain.Company;
import com.sys.virtuele.company.domain.CompanyContact;
import com.sys.virtuele.company.repositories.CompanyContactRepository;
import com.sys.virtuele.company.repositories.CompanyRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CompanyServiceImpl implements CompanyService{

	@Autowired
	private CompanyRepository companyRepository;
	
	@Autowired
	private CompanyContactRepository companyContactRepository;
	
	@Override
	public Optional<CompanyContact> getCompanyContactById(Long companyContactId) {
		return companyContactRepository.findById(companyContactId);
	}

	@Override
	public void deleteCompanyContact(Long companyContactId) {
		companyContactRepository.deleteById(companyContactId);
	}

	@Override
	public List<CompanyContact> getAllCompanyContacts() {
		return companyContactRepository.findAll();
	}

	@Override
	public CompanyContact updateCompanyContact(CompanyContact companyContact) {
		return companyContactRepository.save(companyContact);
	}

	@Override
	public CompanyContact addCompanyContact(CompanyContact companyContact) {
		return companyContactRepository.save(companyContact);
	}

	@Override
	public Optional<Company> getCompanyById(Long companyId) {
		return companyRepository.findById(companyId);
	}

	@Override
	public Company addCompany(Company company) {
		return companyRepository.save(company);
	}

	@Override
	public List<Company> getAllCompanies() {
		return companyRepository.findAll();
	}

	@Override
	public Company updateCompany(Company company) {
		return companyRepository.save(company);
	}

	@Override
	public void deleteCompany(Long companyId) {
		companyRepository.deleteById(companyId);
	}

	@Override
	public List<Company> getAllCompaniesByIds(List<Long> companyIds) {
		return companyRepository.findByIdIn(companyIds);
	}

}
