package com.sys.virtuele.company.services;

import java.util.List;
import java.util.Optional;

import com.sys.virtuele.company.domain.Company;
import com.sys.virtuele.company.domain.CompanyContact;

public interface CompanyService {

	/**
	 * 
	 * Company Contact Related
	 * 
	 */
	public Optional<CompanyContact> getCompanyContactById (Long companyContactId);
	public void deleteCompanyContact(Long companyContactId);
	public List<CompanyContact> getAllCompanyContacts();
	public CompanyContact updateCompanyContact(CompanyContact companyContact);
	public CompanyContact addCompanyContact(CompanyContact companyContact);

	
	/**
	 * 
	 * Company Related
	 * 
	 */
	public Optional<Company> getCompanyById(Long companyId);
	public Company addCompany(Company company);
	public List<Company> getAllCompanies();
	public Company updateCompany(Company company);
	public void deleteCompany(Long companyId);
	public List<Company> getAllCompaniesByIds(List<Long> companyIds);
	
	
	
	
}
