package com.sys.virtuele.company.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.sys.virtuele.common.domain.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class CompanyContact extends BaseEntity implements Serializable{
	


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String primaryContactNumber;
	private String secondaryContactNumber;
	private String addressLine1;
	private String addressLine2;
	private String addressLine3;
	private String city;
	private State state;
	private String zipCode;
	private String country;
	private String primaryEmail;
	private String secondaryEmail;
	

}
