package com.sys.virtuele.company.controllers;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sys.virtuele.company.domain.Company;
import com.sys.virtuele.company.models.UserSecondaryCompany;
import com.sys.virtuele.company.services.CompanyService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/v1/company")
@RestController
public class CompanyController {
	
	@Autowired
	private CompanyService companyService;
	
	@PostMapping
	public ResponseEntity<Company> addCompany(@RequestBody Company company) {
		
		
		Company savedCompany = companyService.addCompany(company);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Location", "/api/v1/company/" + savedCompany.getId());
		
		log.info("<< Added New User !");
		
		return new ResponseEntity<Company>(headers, HttpStatus.CREATED);
	}
	

	
	@GetMapping("/{companyId}")
	public ResponseEntity<Company> getCompanyById(@PathVariable("companyId") Long companyId ) {
		
		log.info(">> "+companyId);
		Optional<Company> company = companyService.getCompanyById(companyId);
		
		log.info("<< Listing Company !");
		
		return new ResponseEntity<>(company.get(), HttpStatus.OK);
	}
	
	@GetMapping
	public ResponseEntity<List<Company>> getAllCompanies() {
		
		List<Company> companyList = companyService.getAllCompanies();
		
		return new ResponseEntity<List<Company>>(companyList , HttpStatus.OK);
	}
	
	@PutMapping
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updateCompany(@RequestBody Company company) {
		
		Company savedCompany= companyService.updateCompany(company);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Location", "/api/v1/company/" + savedCompany.getId());
		
		log.info("<< Updated Existing Company !");
		
	} 
	
	
	@PostMapping("/fetch/companies")
	public ResponseEntity<List<Company>> getCompaniesByCompanyIds(@RequestBody List<UserSecondaryCompany> userSecondaryCompany) {
		
		// Convert List of companyIds to List of Long<companyId> using Lambda Functions
		List<Long> companyIds = userSecondaryCompany.stream().map(UserSecondaryCompany::getCompanyId).collect(Collectors.toList());
		
		List<Company> companyList = companyService.getAllCompaniesByIds(companyIds);

		log.info("<< Fetching Projects based on Project Ids !");
		
		return new ResponseEntity<List<Company>>(companyList, HttpStatus.OK);
	}
}










