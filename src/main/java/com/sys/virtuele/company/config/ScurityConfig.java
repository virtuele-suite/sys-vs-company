package com.sys.virtuele.company.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * 
 * @author pnc
 *
 */


@Configuration
@EnableWebSecurity
public class ScurityConfig extends WebSecurityConfigurerAdapter{

//	@Override
//	protected void configure(HttpSecurity http) throws Exception {
//		
////		http
////		.authorizeRequests(authorize ->{
////			authorize.antMatchers("/").permitAll();
////		})
////		.authorizeRequests()
////		.anyRequest().authenticated()
////		.and()
////		.formLogin().and()
////		.httpBasic();
//		
////		http.authorizeRequests((requests) -> requests.anyRequest().authenticated());
////		http.formLogin();
////		http.httpBasic();
//		
////        http.authorizeRequests()
////        .antMatchers("/api/**")
////        .authenticated()
////        .antMatchers("/api/**")
////        .permitAll()
////        .and()
////        .httpBasic();
//        
//	}
	
	   @Override
	    public void configure(HttpSecurity http) throws Exception {
	       http.csrf().disable().authorizeRequests()
	        .antMatchers("/").permitAll();
	    }
	

}
