package com.sys.virtuele.company.models;

import java.io.Serializable;

import com.sys.virtuele.common.models.BaseDto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class UserSecondaryCompany extends BaseDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6298565309238855531L;

	private Long id;
	
	private Long userId;
	
	private Long companyId;

}
