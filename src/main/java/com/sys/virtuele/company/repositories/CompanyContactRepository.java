package com.sys.virtuele.company.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sys.virtuele.company.domain.CompanyContact;

public interface CompanyContactRepository extends JpaRepository<CompanyContact, Long>{

}
