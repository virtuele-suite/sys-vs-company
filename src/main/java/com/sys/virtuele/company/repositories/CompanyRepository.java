package com.sys.virtuele.company.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sys.virtuele.company.domain.Company;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Long>{
	List<Company> findByIdIn(List<Long> companyIds);
}
